#!/bin/bash
set -e

echo "=========== " $1

if [ "$1" = "ogm-server" ] || [ "$1" = "" ]; then
    wget -O api.zip https://gitlab.com/GISCollective/backend/ogm-server/-/jobs/artifacts/master/download?job=build-fedora-38
    unzip -o api.zip
    ls -lsah deploy

    wget -O defaults.zip https://gitlab.com/GISCollective/backend/ogm-server/-/archive/master/ogm-server-master.zip?path=defaults
    unzip -o defaults.zip
    mv ogm-server-master-defaults/defaults defaults
    rm -rf ogm-server-master-defaults

    wget -O deploy.zip https://gitlab.com/GISCollective/backend/ogm-server/-/archive/master/ogm-server-master.zip?path=deploy
    unzip -o deploy.zip
    mv ogm-server-master-deploy/deploy files
    rm -rf ogm-server-master-deploy
fi

if [ "$1" = "tasks" ] || [ "$1" = "" ]; then
    wget -O tasks.zip https://gitlab.com/GISCollective/backend/tasks/-/jobs/artifacts/master/download?job=build-fedora-38
    unzip -o tasks.zip
    ls -lsah deploy
fi

if [ "$1" = "hmq" ] || [ "$1" = "" ]; then
    wget -O hmq.zip https://gitlab.com/GISCollective/backend/hmq/-/jobs/artifacts/master/download?job=build-fedora-38
    unzip -o hmq.zip
    ls -lsah deploy
fi

rm -rf *.zip

if [ "$1" = "node" ] || [ "$1" = "" ]; then
    git clone https://gitlab.com/GISCollective/backend/node.git
    cd node && yarn && yarn run generate:fonts && cd ..
fi

if [ "$1" = "web-frontend" ] || [ "$1" = "" ]; then
    git clone https://gitlab.com/GISCollective/frontend/web-frontend.git
    cd web-frontend && yarn
    cd packages/gis-collective
    ember build --environment=bundle
    cd dist
    cp ../service/server.js .
    yarn
    cd ../../../../
    pwd && ls
    mv web-frontend/packages/gis-collective/dist web-frontend-dist
    rm -rf web-frontend
    mv web-frontend-dist web-frontend
fi
