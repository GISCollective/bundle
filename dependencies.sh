#!/bin/bash

set -x

dnf install -y gcc-c++ make openssl nodejs wget unzip git git-lfs
npm install -g ember-cli yarn
