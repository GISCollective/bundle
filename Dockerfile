FROM fedora:39

LABEL mantainer="Szabo Bogdan <contact@szabobogdan.com>"

COPY . /app

# Download the apps and Install mongo
RUN chmod +x /app/dependencies.sh && /app/dependencies.sh && \
    dnf install -y gcc-c++ make openssl ImageMagick librsvg2-tools nginx && \
    cd /app && chmod +x /app/prepare.sh && /app/prepare.sh && \
    npm install -g ember-cli yarn && \
    chmod +x /app/mongo.sh && /app/mongo.sh && \
    dnf remove -y gcc-c++ make wget

## Container options

# Service
ENV serviceName GISCollective
ENV serviceUrl http://127.0.0.1:8080

ENV hostName 127.0.0.1:8080
ENV baseUrl http://127.0.0.1:8080
ENV apiUrl http://127.0.0.1:8080/api-v1
ENV httpMqUrl http://127.0.0.1:5000/
ENV pushGatewayUrl ""
ENV mongoUrl mongodb://127.0.0.1:27017/
ENV frontendPort 4200

# Higher number means less messages
ENV logLevel 3

# Mongo
ENV mongoHost localhost
ENV mongoDBName giscollective

# Define data volumes
VOLUME ["/app/config", "/app/data", "/app/logs"]
WORKDIR /app/

# Expose the app port
EXPOSE 8080

# The container entrypoint
ENTRYPOINT ["/app/run.js"]
