#!/bin/bash
set -x

mv deploy/service/*-api-* /app/api
mv deploy/service/*-tasks-* /app/tasks
mv deploy/service/*hmq-* /app/hmq

mkdir -p logs
mkdir -p data
mkdir -p config

rm -rf deploy