#!/bin/node
const { exec } = require('child_process');
const { copyFile } = require('fs/promises');
const { constants } = require('fs');

const batchWaitTime = 1000 * 60 * 60;
const waitTime = 5000;
const rootPath = "/app";

async function ensureConfigExists(name) {
  const source = `/etc/giscollective/${name}`;
  const destination = `/app/config/${name}`;

  try {
    await copyFile(source, destination, constants.COPYFILE_EXCL);
    console.log(`[Bundle] ${source} file was copied to ${destination}`);
  } catch(err) {
    console.log(`[Bundle] ${err}`);
  }
}

function restart(name, command) {
  console.error(`[Bundle][${name}] Service died. Restarting in ${waitTime}ms.` );
  setTimeout(function() {
    startProcess(name, command, restart);
  }, waitTime);
}

function restartLater(name, command) {
  console.error(`[Bundle][${name}] Service ended. Restarting in ${batchWaitTime}ms.` );
  setTimeout(function() {
    startProcess(name, command, restart);
  }, batchWaitTime);
}

function startProcess(name, command, onExit) {
  console.error(`[Bundle][${name}] Starting service.` );

  return new Promise((resolve) => {
    console.log(`[Bundle][COMMAND] ${command}`);
    let hasInitError = false;
    const childProcess = exec(command, function (error, stdout, stderr) {
      if (error) {
        console.log(`[Bundle][${name}] Error code: ${error.code}`);

        if(error.signal) {
          console.log(`[Bundle][${name}] Signal received: ${error.signal}`);
        }

        hasInitError = true;
      }
    });


    childProcess.on('spawn', resolve);

    childProcess.stdout.on('data', (data) => {
      process.stdout.write(data);
    });

    childProcess.stderr.on('data', (data) => {
      process.stderr.write(data);
    });

    childProcess.on('exit', (code) => {
      console.log(`[Bundle][${name}] Child process exited with exit code ${code}`);

      if(!hasInitError) {
        onExit(name, command);
      }
    });
  });
}

async function startServer(name, onExit) {
  await ensureConfigExists(`${name}.json`);
  return startProcess(name, `/app/${name} start -c "${rootPath}/config/${name}.json"`, onExit);
}

async function startFrontend(onExit) {
  return startProcess("Frontend", `cd web-frontend && node ./server.js`, onExit);
}

async function startTasksJs(onExit) {
  await ensureConfigExists(`tasks-js.json`);
  return startProcess("TasksJs", `cd node/packages/tasks && yarn start --configuration "${rootPath}/config/tasks-js.json"`, onExit);
}

async function startFontsJs(onExit) {
  await ensureConfigExists(`fonts-js.json`);
  return startProcess("FontsJs", `cd node/packages/fonts && yarn start --configuration "${rootPath}/config/fonts-js.json"`, onExit);
}

async function startBatchJs(onExit) {
  await ensureConfigExists(`batch-js.json`);
  return startProcess("BatchJs", `cd node/packages/batch && yarn start --configuration "${rootPath}/config/batch-js.json"`, onExit);
}

async function startMongo(onExit) {
  await ensureConfigExists('mongod.conf');
  await ensureConfigExists('mongo.json');
  return startProcess("MongoDb", `/usr/bin/mongod --config ${rootPath}/config/mongod.conf`, onExit);
}

async function startNginx(onExit) {
  await ensureConfigExists('nginx.conf');
  return startProcess("NGINX", `/usr/sbin/nginx -c ${rootPath}/config/nginx.conf`, onExit);
}

async function main() {
  await startMongo(restart);

  await startNginx(restart);

  await startServer('hmq', restart);
  await startServer('api', restart);
  await startServer('tasks', restart);

  await startTasksJs(restart);
  await startFontsJs(restart);
  await startBatchJs(restartLater)

  await startFrontend(restart);
}

process.on('SIGINT', function() {
  console.log( "\nGracefully shutting down from SIGINT (Ctrl-C)" );
  process.exit(1);
});

main().catch(console.error);