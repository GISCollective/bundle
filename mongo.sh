#!/bin/bash

cat > /etc/yum.repos.d/mongodb.repo <<EOF
[mongodb-upstream]
name=MongoDB Upstream Repository
baseurl=https://repo.mongodb.org/yum/redhat/8Server/mongodb-org/4.2/x86_64/
gpgcheck=1
enabled=1
gpgkey=https://www.mongodb.org/static/pgp/server-4.2.asc
EOF

cat /etc/yum.repos.d/mongodb.repo

dnf -y install mongodb-org
mongo -version