worker_processes  1;
error_log  /app/logs/nginx-error.log;
pid        /app/nginx.pid;
worker_rlimit_nofile 8192;
daemon off;

events {
  worker_connections  1024;
}

http {
  default_type application/octet-stream;
  log_format   main '$remote_addr - $remote_user [$time_local]  $status '
    '"$request" $body_bytes_sent "$http_referer" '
    '"$http_user_agent" "$http_x_forwarded_for"';
  access_log   /app/logs/access.log  main;
  sendfile     on;
  tcp_nopush   on;
  server_names_hash_bucket_size 128; # this seems to be required for some vhosts

  server { # simple reverse-proxy
    listen 8080;
    listen [::]:8080 ipv6only=on;
    server_name localhost;
    access_log   /app/logs/nginx-access.log  main;

    # pass requests for dynamic content to rails/turbogears/zope, et al
    location / {
      proxy_pass      http://127.0.0.1:4200/;
      proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
      proxy_set_header Host $http_host;
      proxy_set_header X-Forwarded-Proto $scheme;
    }

    location /api-v1/fonts/ {
      proxy_pass      http://127.0.0.1:9097/;
    }

    location /api-v1/ {
      proxy_pass      http://127.0.0.1:9091/;
    }

    location /auth/ {
      proxy_pass      http://127.0.0.1:9092/;
    }
  }
}